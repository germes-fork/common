package rabbit

import (
	"fmt"
	"sync"

	"github.com/streadway/amqp"
)

var (
	ErrClosedQueue = fmt.Errorf("queue is closed")
)

type ConnectOptions struct {
	Login    string
	Password string
	Host     string
	Port     string
}

type Connection struct {
	amqpConn *amqp.Connection
}

type Queue struct {
	name        string
	connChannel *amqp.Channel
	deliveries  <-chan amqp.Delivery
	bc          chan []byte
	closed      bool
	mu          sync.Mutex
}

func Connect(opt ConnectOptions) (conn *Connection, err error) {
	connStr := fmt.Sprintf("amqp://%s:%s@%s:%s/", opt.Login, opt.Password, opt.Host, opt.Port)
	amqpConn, err := amqp.Dial(connStr)
	if err != nil {
		err = fmt.Errorf("dial amqp: %v", err)
		return
	}
	conn = &Connection{
		amqpConn: amqpConn,
	}
	return
}

func (conn *Connection) GetQueue(name string) (queue *Queue, err error) {
	connChannel, err := conn.amqpConn.Channel()
	if err != nil {
		err = fmt.Errorf("create channel: %v", err)
		return
	}
	_, err = connChannel.QueueDeclare(
		name,  // name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		connChannel.Close()
		err = fmt.Errorf("declare queue: %v", err)
		return
	}
	queue = &Queue{
		name:        name,
		connChannel: connChannel,
	}
	return
}

func (queue *Queue) Publish(b []byte) (err error) {
	err = queue.connChannel.Publish(
		"",         // exchange
		queue.name, // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType: "application/octet-stream",
			Body:        b,
		})
	if err != nil {
		err = fmt.Errorf("publish message: %v", err)
		return
	}
	return
}

func (queue *Queue) Consume() (bc <-chan []byte, err error) {
	queue.mu.Lock()
	defer queue.mu.Unlock()
	if queue.closed {
		err = ErrClosedQueue
		return
	}
	if queue.bc != nil {
		bc = queue.bc
		return
	}
	queue.deliveries, err = queue.connChannel.Consume(
		queue.name, // queue
		"",         // consumer
		true,       // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	if err != nil {
		err = fmt.Errorf("register consumer: %v", err)
		return
	}
	queue.bc = make(chan []byte)
	bc = queue.bc
	go queue.redirect()
	return
}

func (queue *Queue) redirect() {
	for delivery := range queue.deliveries {
		queue.bc <- delivery.Body
	}
	close(queue.bc)
	queue.Close()
}

func (queue *Queue) Close() (err error) {
	err = queue.connChannel.Close()
	queue.mu.Lock()
	queue.closed = true
	queue.mu.Unlock()
	return
}
